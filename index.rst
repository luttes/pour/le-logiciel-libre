
.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>
   <a rel="me" href="https://kolektiva.social/@noamsw"></a>


.. ♀️✊ ⚖️ 📣
.. ✊🏻✊🏼✊🏽✊🏾✊🏿
.. 🤥
.. 🤪
.. ⚖️ 👨‍🎓
.. 🌍 ♀️
.. 🇮🇷
.. 🎥 🎦


.. un·e

|FluxWeb| `RSS <http://luttes.frama.io/pour/le-logiciel-libre/rss.xml>`_

.. _luttes_logiciel_libre:

=================================================
**Luttes pour le Logiciel libre**
=================================================

- https://framalibre.org/
- https://fr.wikipedia.org/wiki/Free_Software_Foundation#Le_logiciel_libre
- :ref:`grenoble_infos:agendas_grenoble`


.. toctree::
   :maxdepth: 3

   news/news
   annuaires/annuaires

